# serialization_cache Module

[Project Page](http://drupal.org/project/serialization_cache)

## Contents

1. [About](#about)
2. [Installation](#installation)
3. [Credits](#credits)

## About

The Drupal 8 serialization system is quite flexible but can be time-consuming when processing all entities recursively, including their fields and embedded entities. This module provides caching for normalized entities and embedded entities by using different caches for different accesses, such as checking "view" permissions for an entity and its fields.

## Installation

1. **Install the module**: Follow the standard installation instructions for Drupal modules. For detailed steps, refer to [Drupal Installation Guide](https://www.drupal.org/node/1897420).

2. **Override the serializer service**: The module overrides the default `serializer` service with `AccessCachingSerializer`. This is done automatically by the module. Alternatively, you can achieve this in your own module by implementing the `ServiceModifierInterface`. Here's an example:

    ```php
    use Drupal\Core\DependencyInjection\ContainerBuilder;
    use Drupal\Core\DependencyInjection\ServiceModifierInterface;
    use Drupal\serialization_cache\AccessCachingSerializer;

    class YourModuleServiceProvider implements ServiceModifierInterface {
      public function alter(ContainerBuilder $container) {
        if ($container->hasDefinition('serializer')) {
          $container->getDefinition('serializer')->setClass(AccessCachingSerializer::class);
        }
      }
    }
    ```

## Credits

For more information, visit the [project page](http://drupal.org/project/serialization_cache).
