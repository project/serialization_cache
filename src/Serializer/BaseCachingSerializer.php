<?php

namespace Drupal\serialization_cache\Serializer;

use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Cache\Context\CacheContextsManager;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\State\StateInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Serializer\Serializer;

/**
 * Base serializer which can cache normalization results.
 */
class BaseCachingSerializer extends Serializer {

  /**
   * The Cache Context Manager service.
   *
   * @var \Drupal\Core\Cache\Context\CacheContextsManager
   */
  protected $cacheContextsManager;

  /**
   * The cache storage.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $cacheStorage;

  /**
   * The request stack service.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The drupal state service.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  protected $cacheContexts = [];
  protected $cacheTags = [];
  protected $recursionLevel = -1;

  protected $cacheMaxAge = Cache::PERMANENT;

  /**
   * BaseCachingSerializer constructor.
   *
   * @param array $normalizers
   * @param array $encoders
   */
  public function __construct(array $normalizers = [], array $encoders = [], CacheBackendInterface $cache_data = NULL, CacheContextsManager $cache_context_manager = NULL, RequestStack $request_stack = NULL, EntityTypeManagerInterface $entity_type_manager = NULL, StateInterface $state = NULL) {
    parent::__construct($normalizers, $encoders);

    $this->cacheStorage = $cache_data;
    $this->cacheContextsManager = $cache_context_manager;
    $this->requestStack = $request_stack;
    $this->entityTypeManager = $entity_type_manager;
    $this->state = $state;
  }

  /**
   * Wraps normalization with tags collection/clearing.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   Entity object.
   * @param callable $normalization_callable
   *   Normalization callable.
   *
   * @return array
   *   Normalization result.
   */
  protected function normalizeInCachingContext(EntityInterface $entity, callable $normalization_callable) {
    $this->recursionLevel++;

    $this->addCollectedCacheTags($entity->getCacheTags());
    $normalized = $normalization_callable();

    $this->recursionLevel--;

    $this->clearCollectedCacheTags();
    return $normalized;
  }

  /**
   * Adds $tags for current recursion level.
   *
   * @param array $tags
   *   Cache tags array.
   */
  protected function addCollectedCacheTags(array $tags) {
    $this->cacheTags[$this->recursionLevel] = Cache::mergeTags(
      $this->getCollectedCacheTags(),
      $tags
    );
  }

  /**
   * Gets cache tags for current recursion level.
   *
   * @return string[]
   *   Cache tags array.
   */
  protected function getCollectedCacheTags() {
    return isset($this->cacheTags[$this->recursionLevel])
      ? $this->cacheTags[$this->recursionLevel]
      : [];
  }

  /**
   * Clears collected tags for current recursion level.
   */
  protected function clearCollectedCacheTags() {
    if (count($this->cacheTags) > $this->recursionLevel + 1) {
      $this->addCollectedCacheTags(
        $this->cacheTags[$this->recursionLevel + 1]
      );
      unset($this->cacheTags[$this->recursionLevel + 1]);
    }
    elseif ($this->recursionLevel === 0 && $this->cacheTags) {
      $this->cacheTags = [];
    }
  }

  /**
   * Gets normalized $entity from cache.
   *
   * @see BaseCachingSerializer::normalize() method.
   *
   * {@inheritdoc}
   *
   * @return bool|array
   *   Normalized data or FALSE if nothing was cached.
   */
  protected function getFromCache(EntityInterface $entity, $format = NULL, array $context = []) {
    $data = $this->cacheStorage->get($this->buildCacheId($entity, $format, $context));
    return $data
      ? $data->data
      : FALSE;
  }

  /**
   * Caches $entity.
   *
   * @param array $normalized
   *   Normalized entity.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   * @param string|null $format
   * @param array $context
   *
   * @see BaseCachingSerializer::normalize() method.
   *
   * @throws \InvalidArgumentException
   */
  protected function setCache(array $normalized, EntityInterface $entity, $format = NULL, array $context = []) {
    $this->cacheStorage->set(
      $this->buildCacheId($entity, $format, $context),
      $normalized,
      $this->maxAgeToExpire(Cache::mergeMaxAges($entity->getCacheMaxAge(), $this->cacheMaxAge)),
      Cache::mergeTags(
        $this->getCollectedCacheTags(),
        $this->getBaseEntityTags($entity)
      )
    );
  }

  /**
   * Builds cache id for $entity.
   *
   * @see BaseCachingSerializer::normalize() method.
   *
   * {@inheritdoc}
   */
  protected function buildCacheId(EntityInterface $entity, $format = NULL, array $context = []) {
    $cid_parts = [
      'normalized',
      $entity->getEntityTypeId(),
      $entity->id(),
      $format ?: 'null',
    ];

    $cid_parts = array_merge(
      $cid_parts,
      $this->cacheContextsManager
        ->convertTokensToKeys($entity->getCacheContexts())
        ->getKeys(),
      $this->getCustomEntityKeys($entity, $format, $context)
    );

    return implode(':', array_filter($cid_parts));
  }

  /**
   * Check if entity can be cached.
   *
   * @param EntityInterface $entity
   *   Entity object.
   *
   * @return bool
   *   Result of the check.
   *
   * @throws PluginNotFoundException
   */
  protected function checkEntity(EntityInterface $entity) {
    static $static = [];

    if (!isset($static[$entity->getEntityTypeId()])) {
      $static[$entity->getEntityTypeId()] = $this->entityTypeManager
        ->getDefinition($entity->getEntityTypeId())
        ->isPersistentlyCacheable();
    }

    return $static[$entity->getEntityTypeId()];
  }

  /**
   * {@inheritdoc}
   *
   * @throws PluginNotFoundException
   * @throws \InvalidArgumentException
   */
  public function normalize($data, $format = NULL, array $context = []): array|string|int|float|bool|\ArrayObject|NULL  {
    if (!$data instanceof EntityInterface || !static::checkEntity($data)) {
      return parent::normalize($data, $format, $context);
    }

    $normalized = $this->normalizeInCachingContext(
      $data,
      function () use ($data, $format, $context) {
        $normalized = $this->getFromCache($data, $format, $context);
        if (!$normalized) {
          $normalized = parent::normalize($data, $format, $context);
          $this->setCache($normalized, $data, $format, $context);
        }

        return $normalized;
      }
    );

    if ($this->state->get('serialization_cache_debug', FALSE)) {
      $normalized['cache_id'] = $this->buildCacheId($data, $format, $context);
    }

    return $normalized;
  }

  /**
   * Maps a $max_age value to an "expire" value for the Cache API.
   *
   * @param int $max_age
   *   A cache max-age value.
   *
   * @return int
   *   A corresponding "expire" value.
   *
   * @throws \InvalidArgumentException
   *
   * @see CacheBackendInterface::set()
   */
  protected function maxAgeToExpire($max_age) {
    return ($max_age === Cache::PERMANENT)
      ? Cache::PERMANENT
      : (int) $this->requestStack->getMasterRequest()->server->get('REQUEST_TIME') + $max_age;
  }

  /**
   * Gets base cache tags for entity.
   *
   * @param EntityInterface $entity
   *   Entity object.
   *
   * @return string[]
   *   List of cache tags.
   */
  protected function getBaseEntityTags(EntityInterface $entity) {
    return [
      $entity->getEntityTypeId() . '_values',
      'entity_field_info',
    ];
  }

  /**
   * Gets some custom cache keys for entity.
   *
   * @see BaseCachingSerializer::normalize() method.
   *
   * {@inheritdoc}
   *
   * @return string[]
   *   List of cache keys.
   */
  protected function getCustomEntityKeys(EntityInterface $entity, $format = NULL, array $context = []) {
    // Custom entity keys.
    return [];
  }

}
