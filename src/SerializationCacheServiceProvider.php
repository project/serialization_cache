<?php

namespace Drupal\serialization_cache;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceModifierInterface;
use Drupal\serialization_cache\Serializer\AccessCachingSerializer;
use Symfony\Component\DependencyInjection\Reference;

/**
 * Class SerializationCacheServiceProvider.
 */
class SerializationCacheServiceProvider implements ServiceModifierInterface {

  /**
   * {@inheritdoc}
   */
  public function alter(ContainerBuilder $container) {
    if ($container->hasDefinition('serializer')) {
      // Change serialize class.
      $container->getDefinition('serializer')
        ->setClass(AccessCachingSerializer::class)
        ->addArgument(new Reference('cache.data'))
        ->addArgument(new Reference('cache_contexts_manager'))
        ->addArgument(new Reference('request_stack'))
        ->addArgument(new Reference('entity_type.manager'))
        ->addArgument(new Reference('state'));
    }
  }

}
