<?php

namespace Drupal\serialization_cache\Serializer;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityInterface;

/**
 * Serializer which caches entities according to user access.
 */
class AccessCachingSerializer extends BaseCachingSerializer {

  /**
   * Gets list of operations to check entity access.
   *
   * @param EntityInterface|NULL $entity
   *
   * @return string[]
   *   Operations list.
   */
  protected function getEntityOperations(EntityInterface $entity = NULL) {
    // @todo: possibly we need the hook here.
    return ['view'];
  }

  /**
   * Gets list of operations to check entity fields access.
   *
   * @param EntityInterface|NULL $entity
   *
   * @return string[]
   *   Operations list.
   */
  protected function getEntityFieldOperations(EntityInterface $entity = NULL) {
    // @todo: possibly we need the hook here.
    return ['view'];
  }

  /**
   * {@inheritdoc}
   */
  protected function getCustomEntityKeys(EntityInterface $entity, $format = NULL, array $context = []) {
    $entity_accesses = [];
    foreach ($this->getEntityOperations($entity) as $operation) {
      if ($entity->access($operation)){
        $entity_accesses[] = $operation;
      }
    }
    $entity_access_key = $entity_accesses
      ? implode(',', $entity_accesses)
      : '';

    $entity_fields_access_key = [];
    if ($entity instanceof ContentEntityInterface) {
      $field_operations = $this->getEntityFieldOperations($entity);
      if ($field_operations) {
        /** @var \Drupal\Core\Field\FieldItemListInterface $field */
        foreach ($entity as $field_name => $field) {
          $field_accesses = [];
          foreach ($field_operations as $operation) {
            if ($field->access($operation, NULL)) {
              $field_accesses[] = $operation;
            }
          }
          if ($field_accesses) {
            $entity_fields_access_key[] = $field_name . implode(',', $field_accesses);
          }
        }
      }
    }

    return array_merge(
      [$entity_access_key],
      $entity_fields_access_key
    );
  }

}
